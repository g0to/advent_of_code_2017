#! /usr/bin/env python3


def evaluate_condition(reg_value, operator, number):
    # type: (int, string, int) -> bool
    if operator == "!=":
        return reg_value != number
    elif operator == ">":
        return reg_value > number
    elif operator == "<":
        return reg_value < number
    elif operator == ">=":
        return reg_value >= number
    elif operator == "<=":
        return reg_value <= number
    else: # operator == "==":
        return reg_value == number


def main():
    # type: () -> None

    highest_historic_reg_value = 0
    lines_itemized = []
    regs = {}
    with open('input') as f:
        for line in f:
            # input format:
            # <reg_name> [inc|dec] [<int>] if <reg_name> [==|!=|>|<|>=|<=] <int>
            #    |          |         |     |     |             |            |
            #    0          1         2     3     4             5            6
            lines_itemized.append(line.rstrip('\n').split())
            regs[lines_itemized[-1][0]] = 0 # init regs values

    for line_itemized in lines_itemized:
        if evaluate_condition(reg_value=regs[line_itemized[4]],
                              operator=line_itemized[5],
                              number=int(line_itemized[6])):
            if line_itemized[1] == "inc":
                regs[line_itemized[0]] += int(line_itemized[2])
            else:
                # decrease
                regs[line_itemized[0]] -= int(line_itemized[2])

        if max(regs.values()) > highest_historic_reg_value:
            highest_historic_reg_value = max(regs.values())


    print(highest_historic_reg_value)


if __name__ == '__main__':
    main()
