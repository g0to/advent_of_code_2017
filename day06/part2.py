#! /usr/bin/env python3


def redistribute_blocks(memory_banks):
    bank_with_most_blocks = memory_banks.index(max(memory_banks))
    remaining_blocks = memory_banks[bank_with_most_blocks]
    memory_banks[bank_with_most_blocks] = 0
    for block in range(bank_with_most_blocks + 1, bank_with_most_blocks + 1 + remaining_blocks):
        memory_banks[block%len(memory_banks)] += 1

    return memory_banks


if __name__ == '__main__':
    with open('input') as f:
        memory_banks = [int(x) for x in f.read().rstrip('\n').split('\t')]

    memory_banks_history = []
    while not memory_banks in memory_banks_history:
        memory_banks_history.append(memory_banks.copy())
        memory_banks = redistribute_blocks(memory_banks)

    print(len(memory_banks_history) - memory_banks_history.index(memory_banks))
