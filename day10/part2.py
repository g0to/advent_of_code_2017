#! /usr/bin/env python3

def main():
    # type: () -> None

    numbers = list(range(0,256))
    numbers_len = len(numbers)
    current_pos = 0
    skip_size = 0
    lengths_suffix=[17, 31, 73, 47, 23]

    with open('input') as f:
        lengths_ascii = [ord(x) for x in f.read()]
    lengths_ascii.extend(lengths_suffix)


    for length in lengths_ascii*64:
        if length > numbers_len:
            continue

        """
        - Reverse the order of that length of elements in the numbers list,
          starting with the element at the current position
        - Move the current position forward by that length plus the skip size
        - Increase the skip size by one
        """

        if (current_pos + length) < numbers_len:
            # we don't need to wrap the end and the beginning of the list

            reversed_numbers = numbers[current_pos:current_pos + length]
            reversed_numbers.reverse()
            numbers[current_pos:current_pos + length] = reversed_numbers
        else:
            # wrapping end and start of the list is needed

            reversed_numbers = numbers[current_pos:] + numbers[:length - len(numbers[current_pos:])]
            reversed_numbers.reverse()
            numbers[current_pos:] = reversed_numbers[:len(numbers[current_pos:])]
            numbers[:len(reversed_numbers[len(numbers[current_pos:]):])] = reversed_numbers[len(numbers[current_pos:]):]

        current_pos = (current_pos + length + skip_size)%numbers_len
        skip_size += 1

    # sparse hash to dense hash conversion
    dense_hash = []
    for i in range(len(numbers)//16):
        acum = numbers[i*16]
        for sparse_digit in numbers[i*16+1:i*16+16]:
            acum = acum ^ sparse_digit
        dense_hash.append(acum)

    dense_hash_hex = ''.join(["{:02x}".format(x) for x in dense_hash])
    print(dense_hash_hex)


if __name__ == '__main__':
    main()
