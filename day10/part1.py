#! /usr/bin/env python3

def main():
    # type: () -> None

    numbers = list(range(0,256))
    numbers_len = len(numbers)
    current_pos = 0
    skip_size = 0
    with open('input') as f:
        lengths = [int(x) for x in f.read().split(',')]

    ######  test

    #numbers = list(range(0,5))
    #numbers_len = len(numbers)
    #lengths = [3,4,1,5]

    ######  test

    for length in lengths:
        if length > numbers_len:
            continue

        """
        - Reverse the order of that length of elements in the numbers list,
          starting with the element at the current position
        - Move the current position forward by that length plus the skip size
        - Increase the skip size by one
        """

        if (current_pos + length) < numbers_len:
            # we don't need to wrap the end and the beginning of the list

            reversed_numbers = numbers[current_pos:current_pos + length]
            reversed_numbers.reverse()
            numbers[current_pos:current_pos + length] = reversed_numbers
        else:
            # wrapping end and start of the list is needed

            reversed_numbers = numbers[current_pos:] + numbers[:length - len(numbers[current_pos:])]
            reversed_numbers.reverse()
            numbers[current_pos:] = reversed_numbers[:len(numbers[current_pos:])]
            numbers[:len(reversed_numbers[len(numbers[current_pos:]):])] = reversed_numbers[len(numbers[current_pos:]):]

        current_pos = (current_pos + length + skip_size)%numbers_len
        skip_size += 1

    print("{}".format(numbers))


if __name__ == '__main__':
    main()
