#! /usr/bin/env python3

valid_passphrases_counter = 0

with open('input') as f:
    for line in f:
        passphrase_words = line.split()
        if len(passphrase_words) == len(set(passphrase_words)):
            valid_passphrases_counter += 1

print(valid_passphrases_counter)
