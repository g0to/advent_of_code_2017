#! /usr/bin/env python3

valid_passphrases_counter = 0

with open('input') as f:
    for line in f:
        passcode_words_sorted = [''.join(sorted(word)) for word in line.split()]
        if len(passcode_words_sorted) == len(set(passcode_words_sorted)):
            valid_passphrases_counter += 1

print(valid_passphrases_counter)
