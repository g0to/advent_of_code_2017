#! /usr/bin/env python3

import re


def main():
    # type: () -> None

    with open('input') as f:
        input_string = f.read()

    # clean canceled characters (!.)
    input_string = re.sub(r'!.', '', input_string)

    # extract garbage groups including delimiters (< >)
    garbage_string = ''.join(re.findall(r'<.*?>', input_string))

    # remove garbage groups delimiters (< >)
    garbage_string = re.sub(r'<(.*?)>', r'\1', garbage_string)

    print(len(garbage_string))


if __name__ == '__main__':
    main()
