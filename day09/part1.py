#! /usr/bin/env python3

import re


def rreplace(s, old_char, new_char):
    # type: (string, char, char) -> string

    reversed_string = s[::-1]
    return reversed_string.replace(old_char, new_char, 1)[::-1]

def main():
    # type: () -> None

    with open('input') as f:
        input_string = f.read()

    # clean canceled characters (!.)
    input_string = re.sub(r'!.', '', input_string)

    # clean garbage (<.*>)
    input_string = re.sub(r'<.*?>', '', input_string)

    # clean characters different than '{' and '}'
    input_string = re.sub(r'[^{|^}]', '', input_string)

    score = 1   # minimum score is 1
    for i in range(1, len(input_string)):
        if input_string[i] == '{':
            # group score equals number of groups that nest it
            score += input_string[:i+1].count('{')
        elif input_string[i] == '}':
            # when group closer is found, nulify group since score has been added already
            # group is nulified by using its closing character '}' and first opening
            # character '{' to its left and replacing both by '#'. That way, they can't
            # be used as nesting groups anymore
            input_string = rreplace(input_string[:i+1], '}', '#') + (input_string[i+1:])
            input_string = rreplace(input_string[:i+1], '{', '#') + (input_string[i+1:])


    print(score)


if __name__ == '__main__':
    main()
