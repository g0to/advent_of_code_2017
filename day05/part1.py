#! /usr/bin/env python3

with open ('input') as f:
    maze = [int(x) for x in f.read().splitlines()]

jump_to = 0
steps_count = 0
while (jump_to >= 0 and jump_to < len(maze)):
    maze[jump_to] += 1
    jump_to += maze[jump_to] - 1
    steps_count += 1

print(steps_count)
