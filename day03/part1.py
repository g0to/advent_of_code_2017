#! /usr/bin/env python2

# # # # # # # # # #
# - Credits to this algorithm, my firestarter to know how to generate the spiral matrix
#   https://rosettacode.org/wiki/Ulam_spiral_(for_primes)#Python
# # # # # # # # # #

from __future__ import print_function, division
from math import ceil, sqrt

def cell_value(n, x, y):
    # in a square bidimensional matrix:
    #   n: matrix size (n x n)
    #   x: abcissa value for cell
    #   y: ordinate value for cell
    d, y, x = 0, y - n//2, x - (n - 1)//2
    l = 2*max(abs(x), abs(y))
    d = (l*3 + x + y) if y >= x else (l - x - y)
    return (l - 1)**2 + d

def generate_spiral_dict(n):
    d = {}
    for y in range(n):
        for x in range(n):
            d[cell_value(n, x, y)] = (y,x)

    return d

def print_matrix(n):
    for y in range(n):
        for x in range(n):
            print(str(cell_value(n, x, y)).rjust(4), end='')
        print()


if __name__ == '__main__':
    max_value = int(raw_input("Enter max value: "))
    spiral_dict = generate_spiral_dict(int(ceil(sqrt(max_value))))
    print_matrix(max_value) # use only with matrixes that fit your screen!

    # the distance between any number in the matrix and 1 is the sum of the
    # absolute difference between that number coordinates and 1's coordinates
    # in the matrix (i.e. the shortest path is the sum of the steps from the
    # x coordinate of a number to the x coordinate of 1 and the y coordinate
    # of a number to the y coordinate of 1)
    print("The lower number of steps to arrive to 1 from", max_value, "is",
        abs(spiral_dict[1][0]-spiral_dict[max_value][0]) + abs(spiral_dict[1][1]-spiral_dict[max_value][1]))
