#!/usr/bin/env python

import sys


def return_list_without_values_greater_than(list_of_ints, threshold_value):
    return [i for i in list_of_ints if i <= threshold_value]

def return_evenly_division(list_of_divisors, divident):
    for divisor in list_of_divisors:
        if divident%divisor == 0:
            return divident/divisor

    return 0

cumulo = 0
with open(sys.argv[1]) as f:
    for line in f:
        row_list = map(int, list(line.split()))
        for number in row_list:
            shorted_list=return_list_without_values_greater_than(row_list, number)
            # By removing the first occurrence of 'number' we avoid that
            # it's divided by itself but leave room to divide by same number
            # if it's repeaded in the list
            shorted_list.remove(number)
            cumulo += return_evenly_division(shorted_list, number)

print cumulo

