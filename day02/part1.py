#!/usr/bin/env python

import sys

cumulo = 0

with open(sys.argv[1]) as f:
	for line in f:
                lista = map(int, list(line.split()))
                cumulo += max(lista) - min(lista)

print cumulo

