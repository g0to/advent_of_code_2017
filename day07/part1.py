#! /usr/bin/env python3

progs_data = []
with open('input') as f:
    for line in f:
        line = line.rstrip('\n')
        prog_name = line.split()[0]
        prog_weigh = line.split()[1]
        if '->' in line:
            prog_dependants = line.split('->')[1].replace(' ','').split(',')
        else:
            prog_dependants = []

        progs_data.append((prog_name, prog_weigh, prog_dependants))

for prog_data in progs_data:
    root_found = True
    for prog_data2 in progs_data:
        if prog_data[0] in prog_data2[2]:
            root_found = False
            break
    if root_found:
        print(prog_data[0])
        break
